from django.apps import AppConfig


class DjMainConfig(AppConfig):
    name = 'dj_main'
